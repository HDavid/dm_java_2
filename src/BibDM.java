import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
      int res = a + b;
        return res;
    }

    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
      if (liste.isEmpty()) {
        return null;
      }
      if (liste.size() == 1) {
        return liste.get(0);
      }
      int mini = liste.get(0);
      for (int nb : liste) {
        if (nb < mini) {
          mini = nb;
        }
      }
      return mini;
    }

    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeur donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
      for (T elem : liste) {
        if (elem.compareTo(valeur) <= 0) {
          return false;
        }
      }
        return true;
    }

    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
      List<T> liste = new ArrayList<>();
      int i = 0;
      int j = 0;
      T last = null;
      while (i < liste1.size() && j < liste2.size()) {
        T elem1 = liste1.get(i), elem2 = liste2.get(j);
        if (last == null || elem1 != last) {
          if (elem1.compareTo(elem2) == 0) {
            liste.add(elem1);
            last = elem1;
            ++i; ++j;
          }else if (elem1.compareTo(elem2) < 0) {
            ++i;
          }else{
            ++j;
          }
        }else{ ++i;}
      }
    return liste;
    }

    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
	    List<String> res = new ArrayList<>();
	    String mot = "";
	    for (int i = 0; i < texte.length(); ++i) {
	      if (texte.charAt(i) != ' ') {
	        mot += texte.charAt(i);
	      }else{
	        if (!mot.equals("")) {
	          res.add(mot);
	          mot = "";
	        }
	      }
	    }
	    if (!mot.equals("")) {
	      res.add(mot);
      }
	    return res;
	  }

    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
	    List<String> listeMots = decoupe(texte);
	    Map<String, Integer> occurence = new HashMap<>();
      Collections.sort(listeMots);
	    for (String mot : listeMots) {
	      if (occurence.containsKey(mot)) {
	        occurence.put(mot, occurence.get(mot) + 1);
	      }else{
	        occurence.put(mot, 0);
	      }
	    }
	    String motMax = null;
	    Integer occurenceMax = 0;
	    Set<String> ensMots = occurence.keySet();
	    List<String> motsTries = new ArrayList<>();
	    for (String mot : ensMots) {
	      motsTries.add(mot);
	    }
	    Collections.sort(motsTries);
	    for (String mot : motsTries) {
	      if (occurence.get(mot) > occurenceMax) {
	        motMax = mot;
	        occurenceMax = occurence.get(mot);
	      }
	    }
	    return motMax;
	  }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
      int cpt = 0;
	    for (int i = 0; i < chaine.length(); ++i) {
	      if (chaine.charAt(i) == '('){
          cpt++;
        }else if (chaine.charAt(i) == ')') {
	        if (cpt == 0) {
            return false;
          }else{
	          cpt--;
          }
	      }
	    }
	    return cpt == 0;
	  }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
      List<String> listeParenthesesCrochets = new ArrayList<>();
	    for (int i = 0; i < chaine.length(); ++i) {
	      String elem = chaine.substring(i, i+1);
	      System.out.println(elem);
	      if (elem.equals("(") || elem.equals("[")){
          listeParenthesesCrochets.add(elem);
        }else if (listeParenthesesCrochets.size() == 0) {
	          return false;
        }
	      int last = listeParenthesesCrochets.size() - 1;
	      if (elem.equals(")")) {
	        if (listeParenthesesCrochets.get(last).equals("(")){
            listeParenthesesCrochets.remove(last);
          }else{
            return false;
          }
	      }else if (elem.equals("]")) {
	        if (listeParenthesesCrochets.get(last).equals("[")) {
	          listeParenthesesCrochets.remove(last);
	        }else{
	          return false;
          }
	      }
	    }
	    return (listeParenthesesCrochets.size() == 0);
    }

    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
      int a = 0;
      int b = liste.size() - 1;
	    while (a < b) {
	      int c = (a + b) / 2;
	      if (liste.get(c).compareTo(valeur) < 0) {
          a = c + 1;
        }else{
          b = c;
        }
	    }
	    return (a < liste.size() && valeur.equals(liste.get(a)));
	  }
}
